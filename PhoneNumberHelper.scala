package com.pligor.phonenumber

import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber
import com.google.i18n.phonenumbers.{NumberParseException, PhoneNumberUtil}
import com.google.i18n.phonenumbers.PhoneNumberUtil.PhoneNumberFormat
import play.api.libs.json._

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
object PhoneNumberHelper {
  private val plusSign = '+'

  val defaultPhoneNumberFormat = PhoneNumberFormat.E164

  implicit val phoneNumberWrites = new Writes[PhoneNumber] {
    def writes(phoneNumber: PhoneNumber): JsValue = JsString(getPhoneString(phoneNumber))
  }

  implicit val phoneNumberReads = new Reads[PhoneNumber] {
    def reads(json: JsValue): JsResult[PhoneNumber] = {
      json.asOpt[JsString].flatMap(jsString => parsePhoneString(jsString.value)).fold(ifEmpty = {
        JsError("").asInstanceOf[JsResult[PhoneNumber]]
      })({
        phoneNumber =>
          JsSuccess(phoneNumber)
      })
    }
  }

  def tryParsePhoneString(phoneString: String, region: Option[String]) = {
    parsePhoneString(
      phoneString = phoneString,
      region = region
    ).map(getPhoneString).getOrElse(phoneString)
  }

  def normalizePhoneString(phoneString: String, region: Option[String] = None): Option[String] = {
    parsePhoneString(phoneString, region).map(getPhoneString)
  }

  def getPhoneString(phoneNumber: PhoneNumber) = {
    PhoneNumberUtil.getInstance().format(phoneNumber, defaultPhoneNumberFormat)
  }

  /**
   * @return "GR", "IT", etc.
   */
  def getCountryCodeFromPhoneNumber(phoneNumber: PhoneNumber): String = {
    PhoneNumberUtil.getInstance().getRegionCodeForNumber(phoneNumber)
  }

  /**
   *
   * @param phoneString E164 phone string with or without plus sign
   * @param region actually country code like GR, UK, etc.
   * @return
   */
  def parsePhoneStringTolerant(phoneString: String, region: Option[String] = None): Option[PhoneNumber] = {
    phoneString.headOption.flatMap {
      firstChar =>

        val prefix = if(firstChar == plusSign) {
          ""
        } else {
          plusSign.toString
        }

        parsePhoneString(prefix + phoneString, region)
    }
  }

  /**
   * @param phoneString must be an E164 phone number (+306956775925) or a plain phone number (6956775925) if region is provided
   * @param region actually country code like GR, UK, etc.
   * @return
   */
  def parsePhoneString(phoneString: String, region: Option[String] = None): Option[PhoneNumber] = {
    try {
      Some(PhoneNumberUtil.getInstance().parse(phoneString, region.getOrElse("")))
    } catch {
      case e: NumberParseException => None
    }
  }

  def isValidPhoneNumber(phoneString: String) = parsePhoneString(phoneString).isDefined

  /**
   * http://katalogoskiniton.com/
   * http://number2provider.e-demos.net/?number=
   */
  def parseGreekCellphone(phoneString: String): Option[String] = {
    val firstNumber = "6"

    val secondNumber = "9"

    val countRestNumbers = 8

    val sixPosition = phoneString.indexOf(firstNumber)

    if (sixPosition == -1) {
      None
    } else {
      val ninePosition = phoneString.indexOf(secondNumber, sixPosition)

      if (ninePosition == -1) {
        None
      } else {
        val stringAfterNine = phoneString.drop(ninePosition + 1)

        val digitsAfterNine = stringAfterNine.filter(_.isDigit)

        if (digitsAfterNine.length < countRestNumbers) {
          None
        } else {
          Some(firstNumber + secondNumber + digitsAfterNine.take(countRestNumbers))
        }
      }
    }
  }

  case class NamePhoneString(name: String, phone: String)

  case class NamePhoneNumber(name: String, phoneNumber: PhoneNumber)
}
